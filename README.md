# Palet

A small application to detect [palet](https://fr.wikipedia.org/wiki/Palet_vend%C3%A9en) positions and be able to determine which ones are the closest to the target.

## Installing

Python 3.7 or above.

Dependencies are managed by Pipenv.

pipenv sync # Add --dev to also install dev dependencies

## Running

`> pipenv run make detect`

## Details

Pictures can be found in `photos` folder.
