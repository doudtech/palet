"""
Loads and displays a video.
"""

# Importing OpenCV
import cv2
import numpy as np
import affichage_data

# Create a VideoCapture object and read from input file
# If the input is the camera, pass 0 instead of the video file name
cap = cv2.VideoCapture(0)
delay_time = 1000  # 1000/delay_time = nb fps. 100 -> 10fps

# Check if camera opened successfully
if not cap.isOpened():
    print("Error opening video stream or file")

# Read the video
while cap.isOpened():
    # Capture frame-by-frame
    ret, frame = cap.read()
    _, output = cap.read()  # output sert à l'affichage des cercles

    # cv2.waitKey(delay_time)

    if ret:

        # Converting the image to grayscale.
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Smoothing without removing edges.
        # gray_filtered = cv2.bilateralFilter(gray, 7, 50, 50)
        gray_filtered = cv2.bilateralFilter(gray, 7, 100, 100)

        # find contours
        # ret, thresh = cv2.threshold(gray_filtered, 127, 255, 1)
        # contours, h = cv2.findContours(thresh, 1, 2)
        # for cnt in contours:
        #    cv2.drawContours(frame, [cnt], 0, (0, 0, 255), 1)

        # Using the Canny filter to get contours
        # edges = cv2.Canny(gray_filtered, 20, 30)
        # Using the Canny filter with different parameters
        # edges_high_thresh = cv2.Canny(gray_filtered, 60, 120)
        # edges_high_thresh = cv2.Canny(gray_filtered, 60, 180) # un rapport de 0.3 fonctionne bien de manière générale

        # compute the median of the single channel pixel intensities
        v = np.median(gray_filtered)
        # A lower value of sigma  indicates a tighter threshold, whereas a larger value of sigma  gives a wider
        # threshold.
        sigma = 0.33
        # apply automatic Canny edge detection using the computed median
        lower = int(max(0, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        edges_high_thresh = cv2.Canny(gray_filtered, lower, upper)

        # Detection de cerlcle
        palets, output = affichage_data.circle_detection(edges_high_thresh, output)

        # Stacking the images to print them together
        # For comparison
        # images = np.hstack((gray, edges, edges_high_thresh))
        # images = np.hstack((output, edges_high_thresh))

        # Display the resulting frame
        cv2.imshow('Frame', output)

        # Press Q on keyboard to  exit
        if cv2.waitKey(25) & 0xFF == ord('q'):
            break

        # Break the loop
    else:
        break

# When everything done, release the video capture object
cap.release()

# Closes all the frames
cv2.destroyAllWindows()
