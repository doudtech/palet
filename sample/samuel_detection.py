from pathlib import Path

import cv2
import os
import numpy as np
import operator
import affichage_data
# Read image

img_path = str(Path(os.path.dirname(__file__), "..", "photos", "photo7.jpeg"))
img = cv2.imread(img_path, 1)

# Convert to HSV ie saturation
img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

# Threshold image
thresh = cv2.threshold(img_hsv, 60, 255, cv2.THRESH_BINARY)[1]

# Get mask with range
mask = cv2.inRange(thresh, (0, 255, 255), (20, 255, 255))
imask = mask > 0

# Create output image
green = np.zeros_like(img_hsv, np.uint8)
green[imask] = img_hsv[imask]

# Write image /!\ cv2 affiche en GBR
cv2.imwrite('../idea/palet_hsv.png', green)

# Eventually plot it /!\ plt.imshow affiche en RGB
# plt.imshow(green)
# plt.show()

# ==========================
# green = cv2.imread('../plate_NB.png')
output = green.copy()
# Convert to gray-scale
gray = cv2.cvtColor(green, cv2.COLOR_BGR2GRAY)
# Blur the image to reduce noise REM: Plus on augmente le blur moins HoughCirlce detectera de faux cercles !!!!!!!!!
gray = cv2.medianBlur(gray, ksize=9)  # ksize = 1, 3, 5, 7, ...

# ==========================
# detect circles in the image
# Photo 20190203_143041  ->  NOK
# circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=1.09, minDist=10, param1=100, param2=100, minRadius=15,
#                               maxRadius=40)
# ======= OK avec THRESH_BINARY
# photo3.jpeg   ->  OK mieux avec blur = 9 et dp >= 6
# circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=9, minDist=10, param1=100, param2=100, minRadius=15,
#                               maxRadius=40)
# photo4.jpeg   ->  OK blur = 9 et dp >= 8.2
# circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=8.2, minDist=10, param1=100, param2=100, minRadius=15,
#                               maxRadius=40)
# photo5.jpeg   ->  OK blur = 9 et dp = 9.7 sauf dp = 9
# circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=9.7, minDist=10, param1=100, param2=100, minRadius=15,
#                               maxRadius=40)
# photo6.jpeg = photo7.jpeg ->  photo6 OK, photo 7 NOK
# circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=8.9, minDist=10, param1=100, param2=100, minRadius=15,
#                               maxRadius=40)
# photo7.jpeg = photo6.jpeg -> photo 7 OK
# circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=11, minDist=10, param1=100, param2=100, minRadius=15,
#                               maxRadius=40)
# photo8.jpeg   ->  OK
# circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=7.5, minDist=10, param1=100, param2=100, minRadius=10,
#                               maxRadius=40)
# photo9.jpeg   ->  NOK blur = 9 dp = 10.2
# circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=10.2, minDist=10, param1=100, param2=100, minRadius=15,
#                            maxRadius=40)
# photo10.jpeg  ->  OK blur = 9 dp = 10
rows, cols = gray.shape
# circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=10, minDist=10, param1=100, param2=100, minRadius=15,
#                           maxRadius=40)
# https://stackoverrun.com/fr/q/3001198
circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=1, minDist=20, param1=50, param2=10, minRadius=20,
                           maxRadius=35)

# ==========================
# le petit est détécteé si param2<=43, mais le calcul est bien plus lent que si param2>60
# /!\ Commencer par toucher au paramètre du filtre de canny pour ne plus avoir un milliard de cercle (ie param 1)
# https://www.learnopencv.com/hough-transform-with-opencv-c-python/
# best param: (gray, cv2.HOUGH_GRADIENT, dp=1.09, minDist=400, param1=20, param2=20, minRadius=60, maxRadius=120)
# best param: (gray, cv2.HOUGH_GRADIENT, dp=1.09, minDist=400, param1=60, param2=35, minRadius=60, maxRadius=120)

print("Cercles =" + str(circles))
# ensure at least some circles were found
if circles is not None:
    # convert the (x, y) coordinates and radius of the circles to integers
    circles = np.round(circles[0, :]).astype("int")
    print("Circles detected :nb = ", len(circles))

    i = 1
    petit = [0, 0, 0, 0]
    # loop over the (x, y) coordinates and radius of the circles
    for (x, y, r) in circles:
        # draw the circle in the output image, then draw a rectangle
        # corresponding to the center of the circle
        cv2.circle(output, (x, y), r, (0, 255, 0), 4)
        cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
        print("Palet n°" + str(i) + " [" + str(x) + ";" + str(y) + "] rayon=" + str(r))
        # Detect le petit
        if r < 25:
            num_petit = i
            petit = [num_petit, x, y, r]
        # Detect le petit
        i += 1
        print("Petit = " + str(petit))

# Création de la matrice palets
palets = np.c_[range(len(circles)), circles, np.ones(circles.shape[0])]
print(palets)

# Detecte le palet le plus proche
print(len(palets))

for ligne in range(len(palets)):
    distance = np.sqrt((petit[1]-palets[ligne][1])**2+(petit[2]-palets[ligne][2])**2)
    print("Palet ", ligne, " est éloigné du petit de ", str(distance))
    palets[ligne][4] = distance

# On classe les palets
sorted_data = list(sorted(palets, key=operator.itemgetter(4)))

affichage_data.palet_gagnant(sorted_data)

affichage_data.liste_palets(sorted_data)
affichage_data.output_distance(sorted_data, output)

#https://moncoachdata.com/blog/devenir-python-ninja-structures-de-donnees/
# Utiliser des listes ?
#http://python-liesse.enseeiht.fr/cours/structures_donnees.html
# Apparement il existe que des tuples, des dict ou des classes
# Pas de tuple, car on ne peut pas modifier des tuples.




# CIRCLES =
# [ [x, y, z]
#   [x, y, z]
#   ...
#   [x, y, z] ]

# show the output image
new_size = 500
img = cv2.resize(img, (new_size, new_size))
img_hsv = cv2.resize(img_hsv, (new_size, new_size))
green = cv2.resize(green, (new_size, new_size))
output = cv2.resize(output, (new_size, new_size))

# cv2.imshow("input", np.hstack([img, img_hsv]))
cv2.imshow("output", np.hstack([img, green, output]))
cv2.waitKey(0)
