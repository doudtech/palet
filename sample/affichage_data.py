import cv2
import numpy as np


def test_print():
    print("TEST")


def circle_detection(frame, output):
    # https://stackoverrun.com/fr/q/3001198
    circles = cv2.HoughCircles(frame, cv2.HOUGH_GRADIENT, dp=1, minDist=20, param1=50, param2=10, minRadius=20,
                               maxRadius=35)

    # ==========================
    # print("Cercles =" + str(circles))
    # ensure at least some circles were found
    if circles is not None:
        # convert the (x, y) coordinates and radius of the circles to integers
        circles = np.round(circles[0, :]).astype("int")
        # print("Circles detected :nb = ", len(circles))

        i = 1

        # loop over the (x, y) coordinates and radius of the circles
        for (x, y, r) in circles:
            # draw the circle in the output image, then draw a rectangle
            # corresponding to the center of the circle
            cv2.circle(output, (x, y), r, (0, 255, 0), 4)
            cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
            # print("Palet n°" + str(i) + " [" + str(x) + ";" + str(y) + "] rayon=" + str(r))
            # Detect le petit
            if r < 25:
                num_petit = i
                petit = [num_petit, x, y, r]
            # Detect le petit
            i += 1
            #print("Petit = " + str(petit))

    palets = np.c_[range(len(circles)), circles, np.ones(circles.shape[0])]
    print(palets)

    # Detecte le palet le plus proche
    print(len(palets))

    petit = ([0, 0, 0, 0])

    for ligne in range(len(palets)):
        distance = np.sqrt((petit[1] - palets[ligne][1]) ** 2 + (petit[2] - palets[ligne][2]) ** 2)
        print("Palet ", ligne, " est éloigné du petit de ", str(distance))
        palets[ligne][4] = distance

    return palets, output


def color_detection(copy_for_color):
    # DETECTION COULEURS
    # loop over the boundaries image en B G R
    # rouge veleda = 158, 75, 81 -> 81, 75, 158
    # bleu pastille : 207, 118, 75
    # bleu veleda : 255, 145, 75
    # lower = [70, 60, 130]
    # upper = [90, 90, 170]
    # lower = [170, 110, 100]
    # upper = [210, 140, 140]
    lower = [180, 90, 70]
    upper = [250, 170, 170]
    # create NumPy arrays from the boundaries
    lower = np.array(lower, dtype="uint8")
    upper = np.array(upper, dtype="uint8")
    # find the colors within the specified boundaries and apply
    # the mask
    mask = cv2.inRange(copy_for_color, lower, upper)
    output_color = cv2.bitwise_and(copy_for_color, copy_for_color, mask=mask)

    return output_color


def output_distance(palets, output):
    # Affichage des data
    for ligne in range(len(palets)):
        cv2.line(output,
                 (int(palets[0][1]), int(palets[0][2])),
                 (int(palets[ligne][1]), int(palets[ligne][2])),
                 (255, 0, 0), 1)
        cv2.putText(output, str(int(palets[ligne][4])),
                    (int(palets[ligne][1]), int(palets[ligne][2])),
                    cv2.FONT_HERSHEY_PLAIN, 1.5, (255, 0, 0), 2)


def liste_palets(palets):
    print(palets)


def palet_gagnant(palets):
    for ligne in range(len(palets)):
        print("Palet ", ligne, " est éloigné du petit de ", str(palets[ligne][4]))
    print("Le palet le plus proche est le palet n°", palets[1][0])
    print(" ", palets[1])


def make_1080p(vc):
    vc.set(3, 1920)
    vc.set(4, 1080)
    return vc


def make_720p(vc):
    vc.set(3, 1280)
    vc.set(4, 720)
    return vc


def make_480p(vc):
    vc.set(3, 640)
    vc.set(4, 480)
    return vc


def change_res(vc, width, height):
    vc.set(3, width)
    vc.set(4, height)
    return vc