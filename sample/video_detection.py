import cv2
import numpy as np
import operator

import affichage_data


cv2.namedWindow("preview")

# define the list of boundaries
boundaries = [
    ([17, 15, 100], [50, 56, 200]),
    ([86, 31, 4], [220, 88, 50]),
    ([25, 146, 190], [62, 174, 250]),
    ([103, 86, 65], [145, 133, 128])
]

vc = cv2.VideoCapture(0)

vc = affichage_data.make_480p(vc)

if vc.isOpened():  # try to get the first frame
    rval, frame = vc.read()
else:
    rval = False

delay_time = 1000  # 1000/delay_time = nb fps. 100 -> 10fps

while rval:

    # vc.set(cv2.CAP_PROP_FPS, 1)
    _, frame = vc.read()  # frame est la vidéo qui est traitée
    _, output = vc.read()  # output sert à l'affichage des cercles
    _, copy_for_color = vc.read()  # copy_for_color sert à la détection des couleurs
    # copy_for_color = cv2.cvtColor(copy_for_color, cv2.COLOR_BGR2HSV)
    # cv2.waitKey(delay_time)

    print(vc.get(cv2.CAP_PROP_FPS))
    print(str(int(vc.get(cv2.CAP_PROP_FRAME_WIDTH))) + "x" + str(int(vc.get(cv2.CAP_PROP_FRAME_HEIGHT))))

    # Convert to HSV ie saturation
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # Threshold image
    frame = cv2.threshold(frame, 60, 255, cv2.THRESH_BINARY)[1]
    # # Get mask with range
    mask = cv2.inRange(frame, (0, 255, 255), (20, 255, 255))
    imask = mask > 0
    # Create output image
    green = np.zeros_like(frame, np.uint8)
    green[imask] = frame[imask]
    # Convert to gray-scale
    frame = cv2.cvtColor(green, cv2.COLOR_BGR2GRAY)
    # Blur the image to reduce noise REM: Plus on augmente le blur moins HoughCirlce detectera de faux cercles !!!!!!!!!
    frame = cv2.medianBlur(frame, ksize=7)  # ksize = 1, 3, 5, 7, ...

    # Detection de cerlcle
    palets, output = affichage_data.circle_detection(frame, output)

    # On classe les palets
    sorted_data = list(sorted(palets, key=operator.itemgetter(4)))

    #color detection
    output_color = affichage_data.color_detection(copy_for_color)

    affichage_data.palet_gagnant(sorted_data)

    # affichage_data.liste_palets(sorted_data)
    # affichage_data.output_distance(sorted_data, frame)

    # output_color ou output
    cv2.imshow("preview", output)

    # Press Q on keyboard to  exit
    if cv2.waitKey(25) & 0xFF == ord('q'):
        break

vc.release()
cv2.destroyWindow("preview")
