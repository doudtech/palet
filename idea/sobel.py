import cv2
import matplotlib.pyplot as plt
import numpy as np
import imutils
import scipy
from scipy import ndimage

## Read image
# img = cv2.imread('../photos/20190203_143041.jpg')
img = cv2.imread('../photos/photo8.jpeg')

## Convert to HSV ie saturation
img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)


# ==========================
# Convert to gray-scale
gray = cv2.cvtColor(img_hsv, cv2.COLOR_BGR2GRAY)
# Blur the image to reduce noise REM: Plus on augmente le blur moins HoughCirlce detectera de faux cercles !!!!!!!!!
gray = cv2.medianBlur(gray, ksize=9)  # ksize = 1, 3, 5, 7, ...

# test sobel
sobel = ndimage.sobel(gray, 0)
cv2.imshow('sobel', sobel)

gray = sobel.copy()
# green = cv2.imread('../plate_NB.png')
output = sobel.copy()

# ==========================
# detect circles in the image
# Photo 20190203_143041  ->  NOK
# circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=1.09, minDist=10, param1=100, param2=100, minRadius=15, maxRadius=40)
# ======= OK avec THRESH_BINARY
# photo3.jpeg   ->  OK mieux avec blur = 9 et dp >= 6
# circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=9, minDist=10, param1=100, param2=100, minRadius=15, maxRadius=40)
# photo4.jpeg   ->  OK blur = 9 et dp >= 8.2
# circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=8.2, minDist=10, param1=100, param2=100, minRadius=15, maxRadius=40)
# photo5.jpeg   ->  OK blur = 9 et dp = 9.7 sauf dp = 9
# circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=9.7, minDist=10, param1=100, param2=100, minRadius=15, maxRadius=40)
# photo6.jpeg = photo7.jpeg ->  photo6 OK, photo 7 NOK
# circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=8.9, minDist=10, param1=100, param2=100, minRadius=15, maxRadius=40)
# photo7.jpeg = photo6.jpeg -> photo 7 OK
# circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=11, minDist=10, param1=100, param2=100, minRadius=15, maxRadius=40)
# photo8.jpeg   ->  OK
circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=7.5, minDist=10, param1=100, param2=100, minRadius=10, maxRadius=40)
# photo9.jpeg   ->  NOK blur = 9 dp = 10.2
# circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=10.2, minDist=10, param1=100, param2=100, minRadius=15, maxRadius=40)
# photo10.jpeg  ->  OK blur = 9 dp = 10
#rows, cols = gray.shape
#circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=10, minDist=10, param1=100, param2=100, minRadius=15, maxRadius=40)

# ==========================
# le petit est détécteé si param2<=43, mais le calcul est bien plus lent que si param2>60
# /!\ Commencer par toucher au paramètre du filtre de canny pour ne plus avoir un milliard de cercle (ie param 1)
# https://www.learnopencv.com/hough-transform-with-opencv-c-python/
# best param: (gray, cv2.HOUGH_GRADIENT, dp=1.09, minDist=400, param1=20, param2=20, minRadius=60, maxRadius=120)
# best param: (gray, cv2.HOUGH_GRADIENT, dp=1.09, minDist=400, param1=60, param2=35, minRadius=60, maxRadius=120)

print("Cercles =" + str(circles))
# ensure at least some circles were found
if circles is not None:
    # convert the (x, y) coordinates and radius of the circles to integers
    circles = np.round(circles[0, :]).astype("int")
    print("Circles detected :nb = ", len(circles))

    i = 1
    petit = [0, 0, 0, 0]
    # loop over the (x, y) coordinates and radius of the circles
    for (x, y, r) in circles:
        # draw the circle in the output image, then draw a rectangle
        # corresponding to the center of the circle
        cv2.circle(output, (x, y), r, (0, 255, 0), 4)
        cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
        print("Palet n°" + str(i) + " [" + str(x) + ";" + str(y) + "] rayon=" + str(r))
        # Detect le petit
        if r < 25:
            num_petit = i
            petit = [num_petit, x, y, r]
        # Detect le petit
        i += 1
        print("Petit = " + str(petit))

# Detecte le palet le plus proche
i = 1
# for i in len(circles):
#   all_distance = np.sqrt((petit[1]*x)+(petit[2]*y))
#  print(str(all_distance))

# show the output image
new_size = 500
img = cv2.resize(img, (new_size, new_size))
img_hsv = cv2.resize(img_hsv, (new_size, new_size))
sobel = cv2.resize(sobel, (new_size, new_size))
gray = cv2.resize(gray, (new_size, new_size))
output = cv2.resize(output, (new_size, new_size))

# cv2.imshow("input", np.hstack([img, img_hsv]))
cv2.imshow("output", np.hstack([img, sobel, gray, output]))
cv2.waitKey(0)
