# USAGE
# python find_shapes.py --image shapes.png

# import the necessary packages
import cv2
import numpy as np
import argparse
import imutils


# construct the argument parse and parse the arguments
# ap = argparse.ArgumentParser()
# ap.add_argument("-i", "--image", help = "path to the image file")
# args = vars(ap.parse_args())

# load the image
# image = cv2.imread(args["image"])
image = cv2.imread("demo_images/20190203_143041.jpg")
if image.shape[1] > 600:
	image = imutils.resize(image, width=600)
# find all the 'black' shapes in the image
lower = np.array([0, 0, 0])
upper = np.array([15, 15, 15])
shapeMask = cv2.inRange(image, lower, upper)

# find the contours in the mask
cnts = cv2.findContours(shapeMask.copy(), cv2.RETR_EXTERNAL,
	cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)
print("I found {} black shapes".format(len(cnts)))
cv2.imshow("Mask", shapeMask)

# loop over the contours
for c in cnts:
	# draw the contour and show it
	cv2.drawContours(image, [c], -1, (0, 255, 0), 2)
	cv2.imshow("Image", image)

	if cv2.waitKey(0) & 0xFF == ord('q'):
		break