import cv2
import matplotlib.pyplot as plt
import numpy as np
import imutils

## Read image
#img = cv2.imread('../photos/20190203_143041.jpg')
img = cv2.imread('../photos/photo10.jpeg')

# Convert to gray-scale
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

blur = cv2.medianBlur(gray, ksize=23) #ksize = 1, 3, 5, 7, ...

th3 = cv2.adaptiveThreshold(blur,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,11,2)

circles = cv2.HoughCircles(th3, cv2.HOUGH_GRADIENT, dp=10, minDist=10, param1=100, param2=100, minRadius=15, maxRadius=40)

output = img.copy()
print("Cercles =" + str(circles))
# ensure at least some circles were found
if circles is not None:
    # convert the (x, y) coordinates and radius of the circles to integers
    circles = np.round(circles[0, :]).astype("int")
    print("Circles detected :nb = ", len(circles))

    i = 1
    petit = [0, 0, 0, 0]
    # loop over the (x, y) coordinates and radius of the circles
    for (x, y, r) in circles:
        # draw the circle in the output image, then draw a rectangle
        # corresponding to the center of the circle
        cv2.circle(output, (x, y), r, (0, 255, 0), 4)
        cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
        print("Palet n°" + str(i) + " [" + str(x) + ";" + str(y) + "] rayon=" + str(r))
        # Detect le petit
        if r < 25:
            num_petit = i
            petit = [num_petit, x, y, r]
        # Detect le petit
        i += 1
        print("Petit = " + str(petit))


# show the output image
new_size = 500
img = cv2.resize(img, (new_size, new_size))
gray = cv2.resize(gray, (new_size, new_size))
blur = cv2.resize(blur, (new_size, new_size))
th3 = cv2.resize(th3, (new_size, new_size))
output = cv2.resize(output, (new_size, new_size))

#cv2.imshow("input", np.hstack([img, img_hsv]))
cv2.imshow("img", img)
cv2.imshow("gray", gray)
cv2.imshow("blur", blur)
cv2.imshow("th3", th3)
cv2.imshow("output", output)

cv2.waitKey(0)

