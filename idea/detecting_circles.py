import cv2
import numpy as np


class Application():

    def resize_and_show(text,image):
            image = cv2.resize(image, (800, 800))
            cv2.imshow(text, image)

    def detecting_circles(self):
        # https://www.pyimagesearch.com/2014/07/21/detecting-circles-images-using-opencv-hough-circles/
        while True:
            img = cv2.imread("../photos/20190203_143041.jpg")
            img = cv2.imread("../photos/coins.jpg")
            # img = cv2.imread("photos/red_circle.jpg")
            output = img.copy()
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            # detect circles in the image
            # jouer avec le coef 1,09 et la distmin de 60
            # coin -> 1,09, 60
            # palet ->
            circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1.2, 100)

            # ensure at least some circles were found
            if circles is not None:
                # convert the (x, y) coordinates and radius of the circles to integers
                circles = np.round(circles[0, :]).astype("int")

                # loop over the (x, y) coordinates and radius of the circles
                for (x, y, r) in circles:
                    # draw the circle in the output image, then draw a rectangle
                    # corresponding to the center of the circle
                    cv2.circle(output, (x, y), r, (0, 255, 0), 4)
                    cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)

                # show the output image
                cv2.imshow("output", np.hstack([img, output]))
            if cv2.waitKey(10) & 0xFF == ord('q'):
                break


if __name__ == '__main__':
    print("Hello World !")
    app = Application()
    app.detecting_circles()
    # http://python-prepa.github.io/ateliers/image_tuto.html
