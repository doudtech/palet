import cv2
from skimage import data, segmentation
from skimage import filters

# import matplotlib.pyplot as plt
# import numpy as np
#
# coins = data.coins()
# coins = cv2.imread("photos/20190203_143041.jpg")
# coins = cv2.cvtColor(coins, cv2.COLOR_BGR2GRAY)
# mask = coins > filters.threshold_otsu(coins)
# clean_border = segmentation.clear_border(mask).astype(np.int)
#
# coins_edges = segmentation.mark_boundaries(coins, clean_border)
#
# plt.figure(figsize=(8, 3.5))
# plt.subplot(121)
# plt.imshow(clean_border, cmap='gray')
# plt.axis('off')
# plt.subplot(122)
# plt.imshow(coins_edges)
# plt.axis('off')
#
# plt.tight_layout()
# plt.show()
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

from skimage import data, filters, feature
from skimage.transform import hough_circle
from skimage.feature import peak_local_max

# Load picture and detect edges
#image = data.coins()[0:95, 70:370]
image = cv2.imread("../photos/20190203_143041.jpg")
image = cv2.imread("palet_hsv.png")
#image = cv2.resize(image, (400, 400))
#cv2.imshow("image",image)
image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#cv2.imshow("GRAY",image)
edges = feature.canny(filters.sobel(image), sigma=2.8)

fig, ax = plt.subplots(ncols=1, nrows=1, figsize=(6, 6))
ax.imshow(image, cmap=plt.cm.gray)

# Detect two radii
radii = np.array([21, 25])
hough_res = hough_circle(edges, radii)

for radius, h in zip(radii, hough_res):
    # For each radius, keep two circles
    maxima = peak_local_max(h, num_peaks=2)
    for maximum in maxima:
        center_x, center_y = maximum - radii.max()
        circ = mpatches.Circle((center_y, center_x), radius, fill=False, edgecolor='red', linewidth=2)
        ax.add_patch(circ)

plt.show()

