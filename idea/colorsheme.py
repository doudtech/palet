import cv2
import numpy as np


class Application():

    def resize_and_show(text,image):
            image = cv2.resize(image, (800, 800))
            cv2.imshow(text, image)

    def detect_red_circle(self):
        while True:
            #img = cv2.imread("photos/20190203_143041.jpg")
            #img = cv2.imread("photos/red_circle.jpg")
            img = cv2.imread("palet_hsv.png")

            img = cv2.GaussianBlur(img, (25, 25), 0)
            Application.resize_and_show('GaussianBlur', img)

            hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
            Application.resize_and_show('GaussianBlur + BGR2HSV', img)

            # Vert/Green G-R>32 , G-B>32
            lower_range = np.array([10, 42, 10], dtype=np.uint8)
            upper_range = np.array([255, 255, 255], dtype=np.uint8)
            # marron 137 108 66
            # lower_range = np.array([125, 90, 55], dtype=np.uint8)
            # upper_range = np.array([145, 115, 75], dtype=np.uint8)
            img = cv2.inRange(hsv, lower_range, upper_range)
            cv2.imwrite('./plate_NB.png', img)
            Application.resize_and_show('Image finale',img)

            if cv2.waitKey(10) & 0xFF == ord('q'):
                break

    def detect_red_circle_coin(self):
        while True:
            img = cv2.imread("../photos/coins.jpg")
            #img = cv2.imread("idea/palet_hsv.png")

            output = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            cv2.imshow('GRAY', output)

            output = cv2.GaussianBlur(output, (25, 25), 0)
            cv2.imshow('GaussianBlur', output)

            img = cv2.cvtColor(output, cv2.COLOR_BGR2HSV)
            cv2.imshow('GaussianBlur + BGR2HSV', img)

            lower_range = np.array([150, 50, 50], dtype=np.uint8)
            upper_range = np.array([200, 255, 255], dtype=np.uint8)
            # lower_range = np.array([80, 60, 20], dtype=np.uint8)
            # upper_range = np.array([110, 85, 40], dtype=np.uint8)
            img = cv2.inRange(output, lower_range, upper_range)
            cv2.imshow('Image finale', img)

            if cv2.waitKey(10) & 0xFF == ord('q'):
                break


if __name__ == '__main__':
    print("Hello World !")
    app = Application()
    #app.detect_red_circle()
    app.detect_red_circle_coin()
    # http://python-prepa.github.io/ateliers/image_tuto.html
