import cv2
import numpy as np
import imutils

image = cv2.imread("../photos/20190203_143041.jpg")

if image.shape[1] > 600:
    image = imutils.resize(image, width=600)

cv2.imshow("input", image)

img = image.copy()
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# Filter
kernel = np.array([[-1, -1, -1],
                   [-1, 4, -1],
                   [-1, -1, -1]])

img = cv2.filter2D(img, -1, kernel)
predicted_contours = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
for contour in predicted_contours:
    cv2.drawContours(img, contour, -1, (0, 255, 0), 3)

cv2.imshow("output", img)
cv2.waitKey(0)
#cv2.imwrite('/Users/s.rochette/Desktop/palet_out.jpeg', img)