from PIL import Image, ImageDraw
from math import sqrt
import numpy as np
import cv2

# Load image:
input_image = Image.open("../photos/photo10.jpeg")
input_pixels = input_image.load()

# Sobel kernels
kernely = [[-1, 0, 1],
           [-2, 0, 2],
           [-1, 0, 1]]
kernelx = [[-1, -2, -1],
           [0, 0, 0],
           [1, 2, 1]]

# Create output image
output_image = Image.new("RGB", input_image.size)
draw = ImageDraw.Draw(output_image)

# Compute convolution between intensity and kernels
for x in range(1, input_image.width - 1):
    for y in range(1, input_image.height - 1):
        magx, magy = 0, 0
        for a in range(3):
            for b in range(3):
                xn = x + a - 1
                yn = y + b - 1
                intensity = sum(input_pixels[xn, yn]) / 3
                magx += intensity * kernelx[a][b]
                magy += intensity * kernely[a][b]

        # Draw in black and white the magnitude
        color = int(sqrt(magx ** 2 + magy ** 2))
        draw.point((x, y), (color, color, color))

output_image.save("sobel.jpeg")

## pour préparer HoughCircles
# Convert to gray-scale
gray = cv2.cvtColor(np.float32(output_image), cv2.COLOR_BGR2GRAY)
# Blur the image to reduce noise REM: Plus on augmente le blur moins HoughCirlce detectera de faux cercles !!!!!!!!!
gray = cv2.medianBlur(gray, ksize=9)  # ksize = 1, 3, 5, 7, ...

## photo8.jpeg   ->  OK
circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, dp=7.5, minDist=10, param1=100, param2=100, minRadius=10, maxRadius=40)