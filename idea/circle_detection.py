import cv2
import numpy as np


class Application():

    def resize_and_show(image):
            image = cv2.resize(image, (800, 800))
            cv2.imshow('Image', image)

    def circle_detection(self):
        # https://github.com/ShubhamCpp/Circle-Detection-in-Real-Time/blob/master/FinalWebcam.py
        while True:
            img = cv2.imread("../photos/20190203_143041.jpg")
            img = cv2.resize(img, (800, 800))
            # img = cv2.imread("photos/coins.jpg")
            # img = cv2.imread("photos/red_circle.jpg")
            output = img.copy()

            # Image processing
            print("Image Processing start...")
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            cv2.imshow('Gray', gray)
            # apply GaussianBlur to reduce noise.
            gray = cv2.GaussianBlur(gray, (5, 5), 0)
            cv2.imshow('Gray + blur', gray)
            # medianBlur is also added for smoothening, reducing noise.
            gray = cv2.medianBlur(gray, 1)
            cv2.imshow('Gray + blur', gray)
            print("... Gaussian Blur...")

            # Adaptive Gaussian Threshold is to detect sharp edges in the Image. For more information Google it.
            # gray = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 3.5)
            # kernel = np.ones((2, 2), np.uint8)
            # gray = cv2.erode(gray, kernel, iterations=1)
            # gray = cv2.dilate(gray, kernel, iterations=1)
            print("... Gaussian Threshold...")

            # detect circles in the image
            circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1.09, 60, param1=30, param2=45, minRadius=20, maxRadius=50)
            # print("Nb of circles detected -> ", len(circles))
            # ensure at least some circles were found
            if circles is not None:
                # convert the (x, y) coordinates and radius of the circles to integers
                circles = np.round(circles[0, :]).astype("int")

                # loop over the (x, y) coordinates and radius of the circles
                for (x, y, r) in circles:
                    # draw the circle in the output image, then draw a rectangle
                    # corresponding to the center of the circle
                    cv2.circle(output, (x, y), r, (0, 255, 0), 4)
                    cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)

                # show the output image
                cv2.imshow('Image', np.hstack([img, output]))
            if cv2.waitKey(10) & 0xFF == ord('q'):
                break


if __name__ == '__main__':
    print("Hello World !")
    app = Application()
    app.circle_detection()
    # http://python-prepa.github.io/ateliers/image_tuto.html
