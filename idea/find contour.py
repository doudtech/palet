import cv2
import numpy as np


class Application():

    def resize_and_show(text,image):
            image = cv2.resize(image, (800, 800))
            cv2.imshow(text, image)

    def find_contour(self):
        while True:
            img = cv2.imread("../photos/20190203_143041.jpg")
            img = cv2.imread("../photos/coins.jpg")
            # img = cv2.imread("photos/red_circle.jpg")

            output = img.copy()
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            Application.resize_and_show('BGR2GRAY', gray)

            gray = cv2.GaussianBlur(gray, (5, 5), 0)
            Application.resize_and_show('BGR2GRAY + GaussianBlur', gray)

            ret, threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
            contours, _ = cv2.findContours(threshold, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
            print("Number of contours detected %d -> "%len(contours))

            for contour in contours:
                area = cv2.contourArea(contour)
                if (area > 1000) & (area < 3000000):
                    print(area)
                    img = cv2.drawContours(img, contour, -1, (0, 255, 0), 3)

            Application.resize_and_show('Image contour',img)

            circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1.3, 60)

            # ensure at least some circles were found
            if circles is not None:
                # convert the (x, y) coordinates and radius of the circles to integers
                circles = np.round(circles[0, :]).astype("int")

                # loop over the (x, y) coordinates and radius of the circles
                for (x, y, r) in circles:
                    # draw the circle in the output image, then draw a rectangle
                    # corresponding to the center of the circle
                    cv2.circle(output, (x, y), r, (0, 255, 0), 4)
                    cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)

                    # show the output image
                    cv2.imshow("output", np.hstack([img, output]))

                if cv2.waitKey(10) & 0xFF == ord('q'):
                    break


if __name__ == '__main__':
    print("Hello World !")
    app = Application()
    app.find_contour()
    # http://python-prepa.github.io/ateliers/image_tuto.html
