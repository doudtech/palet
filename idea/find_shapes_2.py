# USAGE
# python find_shapes.py --image shapes.png

# import the necessary packages
import numpy as np
import argparse
import imutils
import cv2

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", help = "path to the image file")
args = vars(ap.parse_args())

# load the image
# image = cv2.imread(args["image"])
image = cv2.imread('20190203_143041.jpg')

# find all the 'black' shapes in the image range +/-15pix
# Black 0 0 0
lower = np.array([0, 0, 0])
upper = np.array([15, 15, 15])
# Cyan 127 255 238 -> Je detecte le jaune pale
# lower = np.array([112, 240, 223])
# upper = np.array([142, 255, 255])
# palet non central 205 177 128
lower = np.array([180, 150, 110])
upper = np.array([215, 190, 140])
shapeMask = cv2.inRange(image, lower, upper)

# find the contours in the mask
cnts = cv2.findContours(shapeMask.copy(), cv2.RETR_EXTERNAL,
	cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)
print("I found {} black shapes".format(len(cnts)))

if shapeMask.shape[1] > 800:
	shapeMask = imutils.resize(shapeMask, width=800)

cv2.imshow("Mask", shapeMask)

# loop over the contours
for c in cnts:
	# draw the contour and show it
	cv2.drawContours(image, [c], -1, (0, 255, 0), 2)
	image = imutils.resize(image, width=800)
	cv2.imshow("Image", image)
	cv2.waitKey(0)
