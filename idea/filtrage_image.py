# http://python-prepa.github.io/ateliers/image_tuto.html
import cv2
import numpy as np
import filters


class Application():

    def resize_and_show(image):
            image = cv2.resize(image, (800, 800))
            cv2.imshow('Image', image)

    def circle_detection(self):
        # https://github.com/ShubhamCpp/Circle-Detection-in-Real-Time/blob/master/FinalWebcam.py
        while True:
            img = cv2.imread("../photos/20190203_143041.jpg")
            img = cv2.imread("../photos/coins.jpg")
            img = cv2.resize(img, (800, 800))
            # img = cv2.imread("photos/coins.jpg")
            # img = cv2.imread("photos/red_circle.jpg")
            cv2.imshow('Image originale', img)

            # output = cv2.medianBlur(img,3)
            # cv2.imshow('Image median blur', output)


            kernel = np.ones((2, 2), np.uint8)
            # dilate
            output = cv2.dilate(output, kernel, iterations=1)
            cv2.imshow('Image median blur + dilate', output)
            # erode *2
            output = cv2.erode(output, kernel, iterations=2)
            cv2.imshow('Image median blur + dilate + erode*2', output)
            # dilate
            output = cv2.dilate(output, kernel, iterations=1)
            cv2.imshow('Image median blur + dilate + erode*2 + dilate', output)

            output = cv2.cvtColor(output, cv2.COLOR_BGR2GRAY)
            cv2.imshow('Image median blur + erode + gray', output)

            simple_threshold = output > filters.threshold_otsu(output)
            print("!")
            adaptative_threshold = filters.threshold_adaptive(output, 151)
            print("!!")
            filter_res = morphology.remove_small_objects(adaptative_threshold)
            print("!!!")
            clear_image = segmentation.clear_border(filter_res)
            print ("!!!!")
            labels = morphology.label(clear_image, 8, background=0)
            print(labels)
            cv2.imshow('filter_res', labels)
            # cv2.imshow('filter_res', np.hstack([output, clear_image]))

            # circles = cv2.HoughCircles(output, cv2.HOUGH_GRADIENT, 1.09, 150, param1=30, param2=45, minRadius=20, maxRadius=50)
            # # ensure at least some circles were found
            # if circles is not None:
            #     # convert the (x, y) coordinates and radius of the circles to integers
            #     circles = np.round(circles[0, :]).astype("int")
            #
            #     # loop over the (x, y) coordinates and radius of the circles
            #     for (x, y, r) in circles:
            #         # draw the circle in the output image, then draw a rectangle
            #         # corresponding to the center of the circle
            #         cv2.circle(output, (x, y), r, (0, 255, 0), 4)
            #         cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
            #
            #     # show the output image
            #     cv2.imshow('Imgae finale', output)

            if cv2.waitKey(10) & 0xFF == ord('q'):
                break


if __name__ == '__main__':
    print("Hello World !")
    app = Application()
    app.circle_detection()
